<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Livro;
use App\Autor;
use App\Genero;
use App\Editora;
class LivroController extends Controller
{
    public function index() {
       
        $book = Livro::get();
        return view('list-livros', ['book' =>$book]);
    }

    public function salvar(Request $request) {
        $book = new Livro();
        $this ->validate($request,$book->validation);
        $book = $book ->create($request ->all());
        \Session::flash('mensagem_sucesso','Livro cadastrado com sucesso');
        return Redirect::to('livro');
    }

    public function cadastrar() {
        $author = Autor::pluck('nome', 'id');
        $genre = Genero::pluck('descricao', 'id');
        $edithora = Editora::pluck('name', 'id');

        return view('cadastrolivro',compact('author','genre','edithora'));
    }



    public function editar($id) {
        $book = Livro::findOrFail($id);
        $author = Autor::pluck('nome', 'id');
        $genre = Genero::pluck('descricao', 'id');
        $edithora = Editora::pluck('name', 'id');
    
        return view('cadastrolivro', compact('book','author','genre','edithora'));
    }

    public function alterar($id,Request $request) {
        $book = Livro::findOrFail($id); 
        $this ->validate($request,$book->validation);
        $book->update($request->all());
        \Session::flash('mensagem_sucesso','Dados do Livro alterado com sucesso');
        return Redirect::to('livro/');
    }

    public function excluir($id) {
        $book = Livro::findOrFail($id);
        $book->delete();

        \Session::flash('mensagem_sucesso','Livro deletado com sucesso');
        return Redirect::to('livro');
    }


}
