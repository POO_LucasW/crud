<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Genero;

class GeneroController extends Controller
{
    public function index() {
       
        $genre = Genero::get();
        return view('list-generos', ['genre' =>$genre]);
    }

    public function salvar(Request $request) {
        $genre = new Genero();
        $this ->validate($request,$genre->validation);
        $genre = $genre ->create($request ->all());
        \Session::flash('mensagem_sucesso','Genero cadastrado com sucesso');
        return Redirect::to('genero');
    }

    public function cadastrar() {
        
        return view('cadastrogenero');
    }

    public function editar($id) {
        $genre = Genero::findOrFail($id);
        return view('cadastrogenero', ['genre'=>$genre]);
    }

    public function alterar($id,Request $request) {
        $genre = Genero::findOrFail($id); 
        $this ->validate($request,$genre->validation);
        $genre->update($request->all());
        \Session::flash('mensagem_sucesso','Genero alterado com sucesso');
        return Redirect::to('genero/');
    }

    public function excluir($id) {
        $genre = Genero::findOrFail($id);
        $genre->delete();

        \Session::flash('mensagem_sucesso','Genero deletado com sucesso');
        return Redirect::to('genero');
    }
}
