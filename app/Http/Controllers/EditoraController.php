<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Editora;
class EditoraController extends Controller
{
    public function index() {
       
        $edithora = Editora::get();
        return view('list-editoras', ['edithora' =>$edithora]);
    }

    public function salvar(Request $request) {
        $edithora = new Editora();
        $this ->validate($request,$edithora->validation);
        $edithora = $edithora ->create($request ->all());
        \Session::flash('mensagem_sucesso','Editora cadastrada com sucesso');
        return Redirect::to('editora');
    }

    public function cadastrar() {
        
        return view('cadastroeditora');
    }

    public function editar($id) {
        $edithora = Editora::findOrFail($id);
        return view('cadastroeditora', ['edithora'=>$edithora]);
    }

    public function alterar($id,Request $request) {
        $edithora = Editora::findOrFail($id); 
        $this ->validate($request,$edithora->validation);
        $edithora->update($request->all());
        \Session::flash('mensagem_sucesso','Editora alterada com sucesso');
        return Redirect::to('editora/');
    }

    public function excluir($id) {
        $edithora = Editora::findOrFail($id);
        $edithora->delete();

        \Session::flash('mensagem_sucesso','Editora deletada com sucesso');
        return Redirect::to('editora');
    }
}
