<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Autor;

class AutorController extends Controller
{
    public function index() {
       
        $author = Autor::get();
        return view('list-autores', ['author' =>$author]);
    }

    public function salvar(Request $request) {
        $author = new Autor();
        $this ->validate($request,$author->validation);
        $author = $author ->create($request ->all());
        \Session::flash('mensagem_sucesso','Autor cadastrado com sucesso');
        return Redirect::to('autor');
    }

    public function cadastrar() {
       
        return view('cadastro');
    }



    public function editar($id) {
        $author = Autor::findOrFail($id);
        return view('cadastro', ['author'=>$author]);
    }

    public function alterar($id,Request $request) {
        $author = Autor::findOrFail($id); 
        $this ->validate($request,$author->validation);
        $author->update($request->all());
        \Session::flash('mensagem_sucesso','Dados do autor alterado com sucesso');
        return Redirect::to('autor/');
    }

    public function excluir($id) {
        $author = Autor::findOrFail($id);
        $author->delete();

        \Session::flash('mensagem_sucesso','Autor deletado com sucesso');
        return Redirect::to('autor');
    }

}
