<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    protected $fillable = ['descricao'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'generosliterarios';

    public function livro(){

        return $this ->hasMany(Livro::class,'generosliterarios_id');
       
    }
    public $validation = [
        'descricao' =>'required|min:3|max:100',
        
    ];
}
