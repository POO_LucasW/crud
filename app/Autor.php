<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $fillable = ['nome','dtnas','sexo','nacionalidade'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'autores';

    public function livro(){

        return $this ->hasMany(Livro::class,'autores_id');
       
    }

    public $validation = [
        'nome' =>'required|min:3|max:100',
        'dtnas' =>'required|date',
        'sexo' =>'required',
        'nacionalidade' =>'required|min:5|max:100',
    ];
}

