<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livro extends Model
{
    protected $fillable = [
    'titulo',
    'dtlanc',
    'autores_id',
    'editoras_id',
    'generosliterarios_id'
];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'livros';

    public function autor(){

        return $this ->hasMany(Autor::class,'id','autores_id');
       
    }

    public function editora(){

        return $this ->hasMany(Editora::class,'id','editoras_id');
       
    }
    public function generos(){

        return $this ->hasMany(Genero::class,'id','generosliterarios_id');
       
    }

    public $validation = [
        'titulo' =>'required|min:3|max:100',
        'dtlanc' =>'required|numeric'
    ];
}
