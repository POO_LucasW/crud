<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editora extends Model
{
    protected $fillable = ['name'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'editoras';

    public function livro(){

        return $this ->hasMany(Livro::class,'editoras_id');
       
    }
    public $validation = [
        'name' =>'required|min:3|max:100',
        
    ];
}

