@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Livro que deseja cadastrar
                <a class ="float-right btn btn-outline-info" href ="{{url('livro/')}}">Livros Cadastrados</a></div>
                
                @if(isset($errors) && count($errors) >0 )
                <div class="alert alert-danger"> 
                    @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                         @endforeach
                    </div>
                @endif

                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif

                   @if(Request::is('*/editar'))
                  {!! Form::model($book, ['method' => 'PATCH', 'url' => 'livro/'.$book->id])!!}
                   @else
                   {!! Form::open(['url' => 'livro/salvar']) !!}
                   @endif
                   {!!Form::label('titulo','Titulo')!!}
                   
                    {!! Form::input('text','titulo',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'Titulo do Livro']) !!}
                   
                   {!!Form::label('autor','Autores',)!!}
                   
                   {!! Form::select('autores.id', $author,null,['class' =>'form-control', 'autofocus']) !!}

                   {!!Form::label('descricao','Genero',)!!}
                   
                   {!! Form::select('generosliterarios.id', $genre,null,['class' =>'form-control', 'autofocus']) !!}

                   {!!Form::label('editora','Editora',)!!}
                   
                   {!! Form::select('editoras.id', $edithora,null,['class' =>'form-control', 'autofocus']) !!}

                   {!!Form::label('dtlanc','Ano Lançamento')!!}
                   
                    {!! Form::input('number','dtlanc',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'Titulo do Livro', 'min','=','1','max','=','2155']) !!}

                    {!!Form::submit('Salvar',['class'=>'btn btn-primary'])!!}
                </div>

                    {!! Form::close() !!}
                        
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
