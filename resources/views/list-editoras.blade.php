@extends('layouts.app')

@section('content')
<div class="panel-body">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Editora
                <a class ="float-right btn btn-outline-info" href ="{{url('editora/novo')}}">Nova Editora</a></div>
                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif
                <table class ='table'>
                    <th> Nome </th>
                    <th >Acões</th>
                    <tbody>
                    @foreach($edithora as $edithora)
                    <tr>
                    <td>{{ $edithora -> name }}</td>

                    <td >
                    {!! Form::open(['method' => 'DELETE', 'url' => '/editora/'.$edithora->id, 'style' => 'display: inline;'])!!}
                    <button type="submit" class='btn btn-default btn-outline-danger'>Excluir</button>
                    {!! Form::close() !!}
                    <a href="/editora/{{$edithora->id}}/editar" class="btn btn-default btn-outline-warning">Editar</button>
                    </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
            </div>
        </div>
    </div>
</div>
@endsection
