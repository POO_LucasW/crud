@extends('layouts.app')

@section('content')
<div class="panel-body">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Gênero Literário
                <a class ="float-right btn btn-outline-info" href ="{{url('genero/novo')}}">Novo Genero</a></div>
                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif
                <table class ='table'>
                    <th> Descrição </th>
                    <th >Acões</th>
                    <tbody>
                    @foreach($genre as $genre)
                    <tr>
                    <td>{{ $genre -> descricao }}</td>

                    <td >
                    {!! Form::open(['method' => 'DELETE', 'url' => '/genero/'.$genre->id, 'style' => 'display: inline;'])!!}
                    <button type="submit" class='btn btn-default btn-outline-danger'>Excluir</button>
                    {!! Form::close() !!}
                    <a href="/genero/{{$genre->id}}/editar" class="btn btn-default btn-outline-warning">Editar</button>
                    </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
            </div>
        </div>
    </div>
</div>
@endsection
