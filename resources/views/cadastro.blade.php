@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Informe Abaixo os dados do Autor
                <a class ="float-right btn btn-outline-info" href ="{{url('autor/')}}">Lista de Autores Cadastrados</a></div>
                
                @if(isset($errors) && count($errors) >0 )
                <div class="alert alert-danger"> 
                    @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                         @endforeach
                    </div>
                @endif

                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif

                   @if(Request::is('*/editar'))
                  {!! Form::model($author, ['method' => 'PATCH', 'url' => 'autor/'.$author->id])!!}
                   @else
                   {!! Form::open(['url' => 'autor/salvar']) !!}
                   @endif
                  
                   {!!Form::label('nome','Nome')!!}
                   
                    {!! Form::input('text','nome',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'Nome']) !!}
                   
                    {!!Form::label('sexo','Sexo')!!}

                    {!! Form::input('text','sexo',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'---']) !!}

                    
                    {!!Form::label('dtnas','Data de Nascimento')!!}

                    {!! Form::input('date','dtnas',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'data de nascimento']) !!}
                    
                    {!!Form::label('nacionalidade','Nacionalidade')!!}

                    {!! Form::input('text','nacionalidade',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'Nacionalidade']) !!}
                    
                    {!!Form::submit('Salvar',['class'=>'btn btn-primary'])!!}
                </div>

                    {!! Form::close() !!}
                        
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
