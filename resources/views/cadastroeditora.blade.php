@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Editora que deseja cadastrar
                <a class ="float-right btn btn-outline-info" href ="{{url('editora/')}}">Editoras Cadastrados</a></div>
                
                @if(isset($errors) && count($errors) >0 )
                <div class="alert alert-danger"> 
                    @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                         @endforeach
                    </div>
                @endif

                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif

                   @if(Request::is('*/editar'))
                  {!! Form::model($edithora, ['method' => 'PATCH', 'url' => 'editora/'.$edithora->id])!!}
                   @else
                   {!! Form::open(['url' => 'editora/salvar']) !!}
                   @endif
                  
                   {!!Form::label('name','Nome')!!}
                   
                    {!! Form::input('text','name',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'Nome da Editora']) !!}
                    
                    {!!Form::submit('Salvar',['class'=>'btn btn-primary'])!!}
                </div>

                    {!! Form::close() !!}
                        
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
