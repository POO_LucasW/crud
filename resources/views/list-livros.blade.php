@extends('layouts.app')

@section('content')
<div class="panel-body">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Livro
                <a class ="float-right btn btn-outline-info" href ="{{url('livro/novo')}}">Novo Livro</a></div>
                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif
                <table class ='table'>
                    <th> Titulo </th>
                    <th> Genero </th>
                    <th> Editora </th>
                    <th> Ano de Lançamento </th>
                    <th> Autor </th>
                    <th >Acões</th>
                    <tbody>
                    @foreach($book as $book)
                    <tr>
                    @foreach($book -> autor as $autores)
                    @foreach($book -> editora as $editoras)
                    @foreach($book -> generos as $generos)
                    <td>{{ $book -> titulo }}</td>
                    <td>{{ $generos -> descricao}}</td>
                    <td>{{ $editoras -> name }}</td>
                    <td>{{ $book -> dtlanc }}</td>                  
                    <td>{{ $autores -> nome}}</td>   
                    @endforeach   
                    @endforeach 
                    @endforeach           
                    <td class >
                    {!! Form::open(['method' => 'DELETE', 'url' => '/livro/'.$book->id, 'style' => 'display: inline;'])!!}
                    <button type="submit" class='btn btn-default btn-outline-danger'>Excluir</button>
                    {!! Form::close() !!}
                    <a href="/livro/{{$book->id}}/editar" class="btn btn-default btn-outline-warning">Editar</button>
                    </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
            </div>
        </div>
    </div>
</div>
@endsection
