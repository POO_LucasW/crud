@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Genero Literario que deseja cadastrar
                <a class ="float-right btn btn-outline-info" href ="{{url('genero/')}}">Generos Cadastrados</a></div>
                
                @if(isset($errors) && count($errors) >0 )
                <div class="alert alert-danger"> 
                    @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                         @endforeach
                    </div>
                @endif

                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif

                   @if(Request::is('*/editar'))
                  {!! Form::model($genre, ['method' => 'PATCH', 'url' => 'genero/'.$genre->id])!!}
                   @else
                   {!! Form::open(['url' => 'genero/salvar']) !!}
                   @endif
                  
                   {!!Form::label('descricao','Descricao')!!}
                   
                    {!! Form::input('text','descricao',null,['class' =>'form-control', 'autofocus', 'placeholder' =>'Descrição']) !!}
                    
                    {!!Form::submit('Salvar',['class'=>'btn btn-primary'])!!}
                </div>

                    {!! Form::close() !!}
                        
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
