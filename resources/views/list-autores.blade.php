@extends('layouts.app')

@section('content')
<div class="panel-body">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header info">Autores
                <a class ="float-right btn btn-outline-info" href ="{{url('autor/novo')}}">Novo Autor</a></div>
                @if(Session::has('mensagem_sucesso'))
                <div class="alert alert-success"> {{Session::get('mensagem_sucesso')}}</div>
                   @endif
                <table class ='table'>
                    <th> Nome </th>
                    <th> Data de Nasc. </th>
                    <th> Sexo </th>
                    <th> Nacionalidade </th>
                    <th>Ações </th>
                    <tbody>
                    @foreach($author as $author)
                    <tr>
                    <td>{{ $author -> nome }}</td>
                    <td>{{ $author -> dtnas }}</td>
                    <td>{{ $author -> sexo }}</td>
                    <td>{{ $author -> nacionalidade }}</td>
                    <td>
                    
                    {!! Form::open(['method' => 'DELETE', 'url' => '/autor/'.$author->id, 'style' => 'display: inline;'])!!}
                    <button type="submit" class='btn btn-default btn-outline-danger'>Excluir</button>
                    {!! Form::close() !!}
                    <a href="/autor/{{$author->id}}/editar" class="btn btn-default btn-outline-warning">Editar</button>
                    </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
            </div>
        </div>
    </div>
</div>
@endsection
