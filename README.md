Para baixar o Laravel vamos fazer o download de alguns componentes necessários: XAMP e Composer.

Para baixar o xamp vamos ao site e escolher a versão. Utilizei o 7.2.24

https://www.apachefriends.org/download.html

Depois vamos ao site do composer e fazer o download do setup.exe dele.

https://getcomposer.org/download/"

O processo de instalação deve ser feito nessa ordem. Pois o composer vai precisar encontrar o php.exe.

Após instalação de ambos vamos abrir o terminal do seu Sistema Operacional e vamos a instalação.

**composer global require laravel/installer**

Logo após a instalação vamos gerar o pacote de formulários 

**php artisan generate:form**

Pronto nosso passo de instalação está pronto.

Para executar o arquivo vamos clonar o repositório que se encontra em:

bitbucket.org/POO_LucasW/crud/src/master/

É necessário ter o git instalado em sua máquina, para que seja possível fazer o clone do projeto.

Vamos voltar no nosso repositório e clicar em "clone" copiar o link que vai disponibilizar.
Entrar no diretório aonde foi instalado o xamp, ir para pasta htdocs e com botão direito do mouse clicar em "Guit bash here", ao abrir o terminal você cola o link que copiou e espera ao término.

Após clonarmos nosso repositório vamos startar no xamp o mysql e apache e clicar em admin no mysql para abrir a página onde vai ficar nosso banco.
Na página do banco vamos em criar um banco de preferência com a nomenclatura **“biblioteca”** na qual usei e depois vamos em  "Importar" e subir o arquivo .SQL que se encontra na pasta onde foi clonado o repositório.

***OBS: O arquivo necessario para conexão ao banco de dados esta no anexo junto ao e-mail, por segurança ele não vem para o repositorio do bitbucket***

**OBS: Ao fazer o download do .env anexado no e-mail verficar nomeclatura para não ir como .env.example, caso esteja renomear apenas para *.env*. Colocar o arquivo *.env* junto na pasta htdocs**

**Obs: Caso o nome do banco não esteja biblioteca, é necessário alterar o  nome no arquivo .env**

No arquivo .env modificar apenas a nomeclatura. Exemplo abaixo

DB_DATABASE= “nome qual criou o banco” **Não usar aspas**

Após isso vamos voltar ao seu terminal e executar o comando:

**php artisan serve**

Ele vai executar o serv do laravel e te mostrar o ip onde será possível testar o projeto.
Caso venha a dar erro ao fazer o comando acima, pode ser o composer que esteja com versões diferentes. então dentro da pasta do projeto pelo terminal vamos aos comandos.

**composer install**

Logo após vamos tentar starta o serv novamente 

Caso o erro prossiga vamos ao comando

**composer update**

Após startar o serv.
 

