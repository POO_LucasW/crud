<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now salvar something great!
|
*/




Route::group(['midldleware' => 'web'],function(){
Route::get('/','HomeController@index');
    Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/autor','AutorController@index');
Route::get('/autor/novo','AutorController@cadastrar');
Route::get('/autor/{author}/editar','AutorController@editar');
Route::post('/autor/salvar','AutorController@salvar');
Route::patch('/autor/{author}','AutorController@alterar');
Route::delete('/autor/{author}','AutorController@excluir');

Route::get('/genero','GeneroController@index');
Route::get('/genero/novo','GeneroController@cadastrar');
Route::get('/genero/{genre}/editar','GeneroController@editar');
Route::post('/genero/salvar','GeneroController@salvar');
Route::patch('/genero/{genre}','GeneroController@alterar');
Route::delete('/genero/{genre}','GeneroController@excluir');

Route::get('/editora','EditoraController@index');
Route::get('/editora/novo','EditoraController@cadastrar');
Route::get('/editora/{edithora}/editar','EditoraController@editar');
Route::post('/editora/salvar','EditoraController@salvar');
Route::patch('/editora/{edithora}','EditoraController@alterar');
Route::delete('/editora/{edithora}','EditoraController@excluir');

Route::get('/livro','LivroController@index');
Route::get('/livro/novo','LivroController@cadastrar');
Route::get('/livro/{book}/editar','LivroController@editar');
Route::post('/livro/salvar','LivroController@salvar');
Route::patch('/livro/{book}','LivroController@alterar');
Route::delete('/livro/{book}','LivroController@excluir');
});
